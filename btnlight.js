var j5 = require("johnny-five");
var board = new j5.Board();
var LEDPIN = 8;
var BTNPIN = 7;
var ledOn = true;

function updateLed(led) {
  if (ledOn) {
    led.on();
  } else {
    led.off();
  }
}

board.on("ready", function(){
  var led = new j5.Led(LEDPIN);
  var btn = new j5.Button(BTNPIN);
  var servo = new j5.Servo(9);

  // Add servo to REPL (optional)
  this.repl.inject({
    servo: servo
  });

  btn.on("hit", function(){
    ledOn = !ledOn;
    updateLed(led);
  });  

  btn.on("release", function(){
    // ledOn = !ledOn;
    // updateLed(led);
  });

  servo.sweep();
});