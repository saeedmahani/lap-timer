var five = require("johnny-five");
var board = new five.Board();
var Duration = require('duration-js');
var StateMachine = require("javascript-state-machine");
var Firebase = require('firebase');
var moment = require('moment');

var isPressed = false;
// var FORCE_PRESSED_THRESHOLD_RATIO = 0.75;
// var FORCE_UNPRESSED_THRESHOLD_RATIO = 0.1;
var FORCE_PRESSED_THRESHOLD_RATIO = 0.81;
var FORCE_UNPRESSED_THRESHOLD_RATIO = 0.8;
var datePressed = null;
var led;

var fsm = StateMachine.create({
  initial: 'idle',
  events: [
    { name: 'start', from: 'idle', to: 'timing' },
    { name: 'finish', from: 'timing', to: 'idle' },
    { name: 'reset', from: 'timing', to: 'idle' }
  ],
  callbacks: {
    onafterstart: function() {
      datePressed = new Date();
      console.log('GO!');
      led.on();
      fb.set({
        startDate_ms: datePressed.getTime(),
        type: 'start'
      });
    },
    onafterfinish: function() {
      var now = new Date();
      var duration = moment.duration(now - datePressed);
      console.log('FINISH! Time: ', duration.as('seconds').toFixed(2));
      fb.set({
        duration_iso: duration.toISOString(),
        type: 'finish'
      });
      led.off();
    },
    onafterreset: function() {
      console.log('reset. ready to start again.');
    }
  }
});

var FSR_RESTING_PERIOD_IN_MS = 800;

var fsr_fsm = StateMachine.create({
  initial: 'inactive',
  events: [
    { name: 'press', from: 'inactive', to: 'active' },
    { name: 'unpress', from: 'active', to: 'resting' },
    { name: 'doneresting', from: 'resting', to: 'inactive' }
  ],
  callbacks: {
    onenteractive: function() {
      if (fsm.is('idle')) {
        fsm.start();
      }
      else if (fsm.is('timing')) {
        fsm.finish();
      }
    },
    onenterresting: function() {
      setTimeout(function() {
        fsr_fsm.doneresting();
      }, FSR_RESTING_PERIOD_IN_MS);
    }
  }
});

var fb = new Firebase('https://fsr-saeed1.firebaseio-demo.com/timer');

board.on("ready", function(){
  
  led = new five.Led(9);

  // Create a new `fsr` hardware instance.
  var fsr = new five.Sensor({
    pin: "A0",
    freq: 1
  });

  var lastTime = new Date();

  // Scale the sensor's value to the LED's brightness range
  fsr.scale([0, 100]).on("data", function() {
    
    if (fsr_fsm.is('active')) {
      if (this.value / 100 < FORCE_UNPRESSED_THRESHOLD_RATIO) {
        fsr_fsm.unpress();
      }
    } else if (fsr_fsm.is('inactive')) {
      if (this.value / 100 > FORCE_PRESSED_THRESHOLD_RATIO) {
        fsr_fsm.press();
        // led.strobe(500, function() {
        //     led.stop();
        //     led.off();
        // });
      }
    }
    

    if (this.value > 0) {
      // console.info(this.value);
      // var newTime = new Date();
      // console.info(newTime - lastTime);
      // lastTime = newTime;
    }

    // set the led's brightness based on force
    // applied to force sensitive resistor
    // led.brightness(this.value);
  });

});